import java.util.List;
import java.util.Map;

public interface CrudConverter {

    String convertCreateStatement(Map<String,List<String>> operatorToVariablesMap);
}
