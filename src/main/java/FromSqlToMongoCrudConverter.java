import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

public class FromSqlToMongoCrudConverter implements CrudConverter {

    private Map<String, List<String>> map;


    @Override
    public String convertCreateStatement(Map<String, List<String>> map) {
        this.map = map;
        return "db." + collection() + find() + skip() + limit();
    }

    private String collection() {
        return fromArray(map.get("FROM").toArray());
    }

    private String find() {
        String projectionRes  = projection();
        String queryRes = query();
        if(projectionRes.isEmpty()){
            if(queryRes.isEmpty())
                return ".find({})";
            else
                return ".find({" + queryRes + "})";
        }else
            if(queryRes.isEmpty())
                return ".find({}, {"+ projectionRes +"})";
            else return ".find({"+ queryRes + "}, {" + projectionRes +"})";
    }

    private String projection() {
        StringBuilder result = new StringBuilder();
        ListIterator<String> iterator = map.get("SELECT").listIterator();
        while (iterator.hasNext()) {
            String var = iterator.next();
            if (var.equals("*"))
                return "";
            else result.append(var).append(": 1");
            if (iterator.hasNext())
                result.append(", ");
        }

        return String.valueOf(result);
    }

    private String query() {
        if (map.containsKey("WHERE")) {
            return fromArray(map.get("WHERE").toArray()) + ": " + booleanOperator();
        }
        return "";
    }

    private String booleanOperator() {
        if (map.containsKey("=")) {
            return "{$eq: " + fromArray(map.get("=").toArray()) + "}";
        } else if (map.containsKey(">")) {
            return "{$gt: " + fromArray(map.get(">").toArray()) + "}";
        } else if (map.containsKey("<")) {
            return "{$lt: " + fromArray(map.get("<").toArray()) + "}";
        } else if (map.containsKey("<=")) {
            return "{$lte: " + fromArray(map.get("<=").toArray()) + "}";
        } else if (map.containsKey(">=")) {
            return "{$gte: " + fromArray(map.get(">=").toArray()) + "}";
        } else return "";
    }

    private String skip() {
        if (map.containsKey("SKIP"))
            return ".skip(" + fromArray(map.get("SKIP").toArray()) + ")";
        return "";
    }

    private String limit() {
        if (map.containsKey("LIMIT"))
            return ".limit(" + fromArray(map.get("LIMIT").toArray()) + ")";
        return "";
    }

    private String fromArray(Object [] oneStringArray){
        return Arrays.toString(oneStringArray)
                .replace("[","")
                .replace("]","");
    }

}
