import java.util.Objects;
import java.util.function.Predicate;

public class Operator {

    private String name;

    private int orderIndex;

    private Predicate<String> checkIfVariableIsValid;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(int orderIndex) {
        this.orderIndex = orderIndex;
    }

    public void setValidationPredicate(Predicate<String> isVariableValid) {
        this.checkIfVariableIsValid = isVariableValid;
    }

    public boolean checkVariable(String s) {
        return checkIfVariableIsValid.test(s);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Operator operator = (Operator) o;
        return name.equals(operator.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
