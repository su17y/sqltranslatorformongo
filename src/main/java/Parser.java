import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Parser {

    private Map<String, Operator> vocabulary;

    private int[][] matrixOfOperationOrder;

    private Operator currentOperator;

    private String currentVariable;

    private List<String> result = new ArrayList<>();

    public Parser(SqlVocabularyLoader sqlVocabularyLoader) {
        this.vocabulary = sqlVocabularyLoader.loadVocabulary();
        matrixOfOperationOrder = sqlVocabularyLoader.loadMatrixOfOperatorOrder();
        currentOperator = vocabulary.get("START");
        currentVariable = "";
    }

    public List<String> parse(String query) {
        String tempQuery = query + " END"; //add 'end' operator to query
        List<String> tokens = createTokensFromQuery(tempQuery);

        for (String token : tokens) {
            if (isTokenValid(token)) {
                    result.add(token);
            } else{
                if(token.equals("END"))
                    throw new IllegalArgumentException("variable or operator input is missing");
                else
                    throw new IllegalArgumentException("there is error near word " + "\'"
                    + token + "\'" + " in " + "\'" + query + "\'");}
        }
        result.remove("END");
        return result;
    }

    public Map<String, List<String>> getMapFromTokens(List<String> tokens) {
        Map<String, List<String>> result = new HashMap<>();
        List<String> variables = new ArrayList<>();
        tokens.removeIf(s -> s.equals(","));

        int current = 0;
        while (current < tokens.size()) {
           if(vocabulary.containsKey(tokens.get(current))){
               String operation = tokens.get(current);
               current++;
               while (current < tokens.size() && !vocabulary.containsKey(tokens.get(current))){
                   variables.add(tokens.get(current));
                   current++;
               }
               result.put(operation,variables);
               variables = new ArrayList<>();
           }
        }
        return result;
    }


    private List<String> createTokensFromQuery(String query) {
        String[] tokens = query.split("\\s");
        return splitByComma(tokens);
    }

    private List<String> splitByComma(String[] tokens) {
        List<String> result = new ArrayList<>();
        for (String token : tokens) {
            if (!token.contains(",")) {
                result.add(token);
            } else {
                String word = token.replace(",", "");
                result.add(word);
                result.add(",");
            }
        }
        return result;
    }


    private boolean isTokenValid(String token) {
        if (vocabulary.containsKey(token)) {
            return trySetCurrentOperation(vocabulary.get(token));
        } else {
            return trySetCurrentVariable(token);
        }
    }

    private boolean trySetCurrentOperation(Operator operator) {
        if (isValidOperation(operator)) {
            currentOperator = operator;
            refreshVariableInput();
            return true;
        }
        return false;
    }

    private boolean trySetCurrentVariable(String variable) {
        if (isValidVariable(variable)) {
            currentVariable = variable;
            return true;
        } else return false;
    }

    private boolean isValidOperation(Operator operator) {
        return matrixOfOperationOrder[currentOperator.getOrderIndex()][operator.getOrderIndex()] == 1
                && isEndOfVariablesInput();
    }

    private boolean isValidVariable(String variable) {
        if (currentOperator.checkVariable(variable)) {
            currentVariable = variable;
            return true;
        } else return false;
    }

    private boolean isEndOfVariablesInput() {
        if (currentVariable == null) {
            throw new IllegalArgumentException("variable is missing near "
                    + "\'" + currentOperator.getName() + "\'");
        } else return true;
    }

    private void refreshVariableInput() {
        currentVariable = null;
    }
}
