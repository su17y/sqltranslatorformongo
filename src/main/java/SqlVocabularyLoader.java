import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.function.Predicate;
import java.util.regex.Pattern;

public class SqlVocabularyLoader implements VocabularyLoader {

    private Map<String, Integer> operatorIndex = new HashMap<>();

    private Map<String, List<String>> operatorPredicateMap = new HashMap<>();

    private Pattern onlyNumbers = Pattern.compile("[0-9]");

    private Pattern onlyLetters = Pattern.compile("[A-Za-z]");

    private Pattern onlyNumbersAndLetters = Pattern.compile("[A-Za-z_0-9]");

    @Override
    public Map<String, Operator> loadVocabulary() {
        return createVocabulary();
    }

    @Override
    public int[][] loadMatrixOfOperatorOrder() {
        return createMatrixOfOperatorOrder();
    }

    private Map<String, Operator> createVocabulary() {
        loadOperatorIndexMap();
        loadVariablePredicateMap();
        Map<String, Operator> vocabulary = new HashMap<>();
        for (String operatorName : operatorIndex.keySet()) {
            Operator operator = new Operator();
            operator.setName(operatorName);
            operator.setOrderIndex(operatorIndex.get(operatorName));
            operator.setValidationPredicate(createOperatorPredicate(operatorName));
            vocabulary.put(operatorName, operator);
        }
        return vocabulary;
    }

    private int[][] createMatrixOfOperatorOrder() {
        String file = "src/main/resources/order.txt";
        Scanner scanner = null;
        try {
            scanner = new Scanner(new File(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        int[][] matrix = new int[13][13];
        do {
            String[] values = scanner.nextLine().split(" ");
            for (int i = 1; i < values.length; i++) {
                matrix[operatorIndex.get(values[i])][operatorIndex.get(values[0])] = 1;
            }
        } while (scanner.hasNext());
        return matrix;
    }


    private void loadOperatorIndexMap() {
        String file = "src/main/resources/vocabulary.txt";
        Scanner scanner = null;
        try {
            scanner = new Scanner(new File(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        do {
            String[] values = scanner.nextLine().split(" ");
            operatorIndex.put(values[0], Integer.valueOf(values[1]));
        } while (scanner.hasNext());

    }

    private void loadVariablePredicateMap() {
        String file = "src/main/resources/variable.txt";
        Scanner scanner = null;
        try {
            scanner = new Scanner(new File(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        do {
            String[] values = scanner.nextLine().split(" ");
            List<String> predicateNames = new ArrayList<>();
            for (int i = 1; i < values.length; i++) {
                predicateNames.add(values[i]);
            }
            operatorPredicateMap.put(values[0], predicateNames);
        } while (scanner.hasNext());
    }


    private Predicate<String> createOperatorPredicate(String operator) {
        Predicate<String> defaultPredicate = this::defaultPredicate;
        for (String predicate : operatorPredicateMap.get(operator)) {
            switch (predicate) {
                case "NONE":
                    defaultPredicate = defaultPredicate.or(this::nonePredicate);
                case "WORD":
                    defaultPredicate = defaultPredicate.or(this::wordPredicate);
                case "STAR":
                    defaultPredicate = defaultPredicate.or(this::starPredicate);
                case "DIGIT":
                    defaultPredicate = defaultPredicate.or(this::digitPredicate);
                case "CHARWORD":
                    defaultPredicate = defaultPredicate.or(this::charWordPredicate);
                default:
                    break;
            }
        }
        return defaultPredicate;
    }

    private boolean starPredicate(String s) {
        return s.equals("*");
    }

    private boolean wordPredicate(String s) {
        if (!onlyLetters.matcher(String.valueOf(s.charAt(0))).find()) {
            return false;
        }
        for (int i = 1; i < s.length(); i++) {
            if (!onlyNumbersAndLetters.matcher(String.valueOf(s.charAt(i))).find()) {
                return false;
            }
        }
        return true;
    }

    private boolean digitPredicate(String s) {
        for (int i = 0; i < s.length(); i++) {
            if (!onlyNumbers.matcher(String.valueOf(s.charAt(i))).find()) {
                return false;
            }
        }
        return true;
    }

    private boolean charWordPredicate(String s){

        for (int i = 1; i < s.length()-1; i++) {
            if (!onlyNumbersAndLetters.matcher(String.valueOf(s.charAt(i))).find()) {
                return false;
            }
        }
        return s.charAt(0) == '\'' && s.charAt(s.length() -1) == '\'';
    }

    private boolean nonePredicate(String s) {
        return s.isEmpty();
    }

    private boolean defaultPredicate(String s) {
        return false;
    }
}
