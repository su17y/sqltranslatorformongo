public class Translator {

    private Parser parser = new Parser(new SqlVocabularyLoader());

    private CrudConverter converter = new FromSqlToMongoCrudConverter();

    public String translate(String query){

        return converter.convertCreateStatement(parser.getMapFromTokens(parser.parse(query)));
    }

}
