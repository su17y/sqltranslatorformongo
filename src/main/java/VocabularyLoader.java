import java.util.Map;

public interface VocabularyLoader {

    Map<String, Operator> loadVocabulary();

    int[][] loadMatrixOfOperatorOrder();
}
