import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class ParserTest {

    private Parser parser = new Parser(new SqlVocabularyLoader());

    private String getErrorMessage(String s1, String s2){
        return "there is error near word " + "\'"
                + s1 + "\'" + " in " + "\'" + s2 + "\'";
    }
    @Test
    public void testValidSqlSelectAllFromCountry(){
        List<String> expected = new ArrayList<>();
        expected.add("SELECT");
        expected.add("*");
        expected.add("FROM");
        expected.add("country");
        assertEquals(expected,parser.parse("SELECT * FROM country"));
    }

    @Test
    public void testValidSqlSelectCityFromCountry(){
        List<String> expected = new ArrayList<>();
        expected.add("SELECT");
        expected.add("city");
        expected.add("FROM");
        expected.add("country");
        assertEquals(expected,parser.parse("SELECT city FROM country"));
    }

    @Test
    public void testValidSqlSelectCityAndPopulationFromCountry(){
        List<String> expected = new ArrayList<>();
        expected.add("SELECT");
        expected.add("city");
        expected.add(",");
        expected.add("population");
        expected.add("FROM");
        expected.add("country");
        assertEquals(expected,parser.parse("SELECT city, population FROM country"));
    }

    @Test
    public void testValidSqlSelectWherePopulationEqual(){
        List<String> expected = new ArrayList<>();
        expected.add("SELECT");
        expected.add("*");
        expected.add("FROM");
        expected.add("country");
        expected.add("WHERE");
        expected.add("population");
        expected.add("=");
        expected.add("10000");
        assertEquals(expected,parser.parse("SELECT * FROM country WHERE population = 10000"));
    }

    @Test
    public void testValidSqlSelectWherePopulationIsGreater(){
        List<String> expected = new ArrayList<>();
        expected.add("SELECT");
        expected.add("*");
        expected.add("FROM");
        expected.add("country");
        expected.add("WHERE");
        expected.add("population");
        expected.add(">");
        expected.add("21000");
        assertEquals(expected,parser.parse("SELECT * FROM country WHERE population > 21000"));
    }

    @Test
    public void testValidSqlSelectWherePopulationIsLessOrEqual(){
        List<String> expected = new ArrayList<>();
        expected.add("SELECT");
        expected.add("*");
        expected.add("FROM");
        expected.add("country");
        expected.add("WHERE");
        expected.add("population");
        expected.add("<=");
        expected.add("50000");
        assertEquals(expected,parser.parse("SELECT * FROM country WHERE population <= 50000"));
    }

    @Test
    public void testValidSqlSelectAllLimit(){
        List<String> expected = new ArrayList<>();
        expected.add("SELECT");
        expected.add("*");
        expected.add("FROM");
        expected.add("country");
        expected.add("LIMIT");
        expected.add("10");
        assertEquals(expected,parser.parse("SELECT * FROM country LIMIT 10"));
    }

    @Test
    public void testValidSqlSelectAllSkip(){
        List<String> expected = new ArrayList<>();
        expected.add("SELECT");
        expected.add("*");
        expected.add("FROM");
        expected.add("country");
        expected.add("SKIP");
        expected.add("13");
        assertEquals(expected,parser.parse("SELECT * FROM country SKIP 13"));
    }

    @Test
    public void testInvalidSqlSelectAll(){
        Throwable thrown = catchThrowable(() -> parser.parse("SELECT *city FROM country"));
        assertThat(thrown).isInstanceOf(IllegalArgumentException.class);
        assertEquals(thrown.getMessage(),getErrorMessage("*city","SELECT *city FROM country"));
    }

    @Test
    public void testInvalidSqlSelectCity(){
        Throwable thrown = catchThrowable(() -> parser.parse("SELECT c;ity FROM country"));
        assertThat(thrown).isInstanceOf(IllegalArgumentException.class);
        assertEquals(thrown.getMessage(),getErrorMessage("c;ity","SELECT c;ity FROM country"));
    }

    @Test
    public void testInvalidSqlSelectCityAndPopulation(){
        Throwable thrown = catchThrowable(() -> parser.parse("SELECT city, population, FROM country"));
        assertThat(thrown).isInstanceOf(IllegalArgumentException.class);
        assertEquals(thrown.getMessage(),"variable is missing near ','");
    }

    @Test
    public void testInvalidSqlWhereWithoutBooleanOperator(){
        Throwable thrown = catchThrowable(() -> parser.parse("SELECT city, population FROM country WHERE age"));
        assertThat(thrown).isInstanceOf(IllegalArgumentException.class);
        assertEquals(thrown.getMessage(),"variable or operator input is missing");
    }

    @Test
    public void testInvalidSqlFromSelect(){
        Throwable thrown = catchThrowable(() -> parser.parse("FROM city SELECT"));
        assertThat(thrown).isInstanceOf(IllegalArgumentException.class);
        assertEquals(thrown.getMessage(),getErrorMessage("FROM","FROM city SELECT"));
    }

    @Test
    public void testInvalidSqlSelectLimitFrom(){
        Throwable thrown = catchThrowable(() -> parser.parse("SELECT city LIMIT 10"));
        assertThat(thrown).isInstanceOf(IllegalArgumentException.class);
        assertEquals(thrown.getMessage(),getErrorMessage("LIMIT","SELECT city LIMIT 10"));
    }

    @Test
    public void testInvalidSqlSelectFromLimitWhere(){
        Throwable thrown = catchThrowable(() -> parser.parse("SELECT city FROM country LIMIT 10 WHERE age > 10"));
        assertThat(thrown).isInstanceOf(IllegalArgumentException.class);
        assertEquals(thrown.getMessage(),getErrorMessage("WHERE","SELECT city FROM country LIMIT 10 WHERE age > 10"));
    }

}
