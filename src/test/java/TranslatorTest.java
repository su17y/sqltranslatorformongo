import org.junit.Test;

import static org.junit.Assert.*;
public class TranslatorTest {

    private Translator translator = new Translator();

    @Test
    public void testSelectAllFromSalesLimit(){
        assertEquals("db.sales.find({}).limit(10)",
                translator.translate("SELECT * FROM sales LIMIT 10"));
    }

    @Test
    public void testSelectNameAndSurnameFromCollection(){
        assertEquals("db.collection.find({}, {name: 1, surname: 1})",
                translator.translate("SELECT name, surname FROM collection"));
    }

    @Test
    public void testSelectAllSkipAndLimit(){
        assertEquals("db.collection.find({}).skip(5).limit(10)",
                translator.translate("SELECT * FROM collection SKIP 5 LIMIT 10"));
    }

    @Test
    public void testSelectAllWhereAgeIsGreater(){
        assertEquals("db.customers.find({age: {$gt: 22}})",
                translator.translate("SELECT * FROM customers WHERE age > 22"));
    }

    @Test
    public void testSelectNameSurnameAndSexFromCollection(){
        assertEquals("db.collection.find({}, {name: 1, surname: 1, sex: 1})",
                translator.translate("SELECT name, surname, sex FROM collection"));
    }

    @Test
    public void testSelectNameSurnameFromCollectionWhereAgeIsGreaterOrLess(){
        assertEquals("db.collection.find({age: {$gte: 50}}, {name: 1, surname: 1})",
                translator.translate("SELECT name, surname FROM collection WHERE age >= 50"));
    }

    @Test
    public void testSelectNameSurnameFromCollectionWhereNameIsIvan(){
        assertEquals("db.collection.find({name: {$eq: 'Ivan'}}, {name: 1, surname: 1})",
                translator.translate("SELECT name, surname FROM collection WHERE name = 'Ivan'"));
    }
}
